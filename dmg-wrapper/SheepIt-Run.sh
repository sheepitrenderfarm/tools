#!/bin/bash
set -euo pipefail # Unofficial strict mode, see http://redsymbol.net/articles/unofficial-bash-strict-mode/

cd "${0%/*}"
# see https://stackoverflow.com/questions/3349105/how-can-i-set-the-current-working-directory-to-the-directory-of-the-script-in-ba

if [ -e "./DEBUG" ]; then
    set -x
    DEBUG="1"
    rm -rf "$HOME/Library/Application Support/SheepIt Launcher"
else
    DEBUG="0"
fi

# This is bash on macOS, some stuff works different
BIN=""
APP="SheepIt"
LAUDIR="$HOME/Library/Application Support/SheepIt Launcher"
ARCH="$(uname -m)"
REPO="https://api.github.com/repos/adoptium/temurin21-binaries/releases"
if [ "$ARCH" = "x86_64" ]; then
    JRE_ARCH="x64"
elif [ "$ARCH" = "arm64" ]; then
    JRE_ARCH="aarch64"
else
    echo "What the fudge, what kind of Mac are you running this under, PowerPC? Get outta here!"
    exit 1
fi
echo "NOTICE:"
echo "Please only pin the first window - the launcher to the Dock"
if [ ! -e "./$APP" ]; then # If JRE exists in the dmg, don't download it
    mkdir -p "$LAUDIR"
    BIN="$LAUDIR/$APP"
    JREVERSION="$LAUDIR/JRE-Version"
    if [ "$(find "$LAUDIR" -iname "jdk*" -type d -depth 1 -exec printf %c {} + | wc -c)" -gt 1 ]; then # If we have multiple jdk dirs in the SheepIt Launcher dir, remove them
        rm -rf "$LAUDIR/jdk"* "$BIN" "$JREVERSION" || true
    fi
    echo "Checking for JRE updates"
    # Let me explain this before it gets unexplainable/unmaintainable
    # sed -nE
    # -n aka --quiet for not printing our entire pattern space, more on that later
    # -E aka --regexp-extended is self-explanatory, note, this ain't your luxurious PCRE, this is what we can work with:
    # https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions
    # 's|regexp|replacement|p'
    # s is substitute mode, for more on that, consult `man sed(1)`
    # | is our separator, sed in s mode is often used with / as a separator, we don't since we are working with URLs and paths
    # regexp is the regex, I can't explain that here, read up on it or spare yourself the suffering
    # replacement is in our case \1 which is the first capture group, the stuff in ()
    # p instructs sed to print our match and since we used -n, only our match
    # Using different types of quoted strings like ''"" after one another, it concatenates them
    JRE_SED_PATTERN='s|.*\"(https.*OpenJDK.*jre.*'"$JRE_ARCH"'.*mac.*\.tar\.gz)\".*|\1|p'
    JRE_URL="$(curl --connect-timeout 20 --retry 4 --silent --show-error $REPO | sed -nE "$JRE_SED_PATTERN" | head -n 1)"
    JRE_LATEST="$(echo "$JRE_URL" | sed -nE 's|.*/(OpenJDK.*)$|\1|p')" # Separate filename after last slash
    if [ -z "$JRE_LATEST" ]; then
        # Empty JRE_LATEST is a good indicator of a critical failure
        echo "!!! Failed parsing JRE information! Aborting !!!"
        echo "Possible causes and troubleshooting steps:"
        echo "1. Check for internet connectivity \(Routes, DNS, Proxy\)"
        echo "2. Github or the JRE repository may be down/unavailable."
        echo "   This might require a new version of the launcher from the SheepIt website: https://www.sheepit-renderfarm.com/"
        echo "3. Open an issue on GitLab/Ask on the Discord if problems persists after 1. and 2."
        exit 4
    elif [[ ! -f "$JREVERSION" || "$JRE_LATEST" != $(cat "$JREVERSION") ]]; then
        echo "Updates detected, updating JRE to $JRE_LATEST"
        rm -rf "$LAUDIR/jdk"* "$BIN" || true # The "|| true" is that we expect the command to fail (If there is nothing to remove), needed since we are running in strict mode
        echo "$JRE_LATEST" > "$JREVERSION"
        curl --connect-timeout 20 --retry 4 -# -L -o "$LAUDIR/$JRE_LATEST" "$JRE_URL"
        tar -xf "$LAUDIR/$JRE_LATEST" -C "$LAUDIR"
        rm "$LAUDIR/$JRE_LATEST"
        ln -s "$(find "$LAUDIR" -name java)" "$BIN" # We symlink java to make the title appearing in the dock to read "SheepIt", not "java"
    else
        echo "No JRE updates detected"
    fi
    if [ ! -e "${BIN}" ]; then # The symlink and its target should exist, but apparently this likes to break, let's check and recreate it if necesseary
        rm -f "$BIN" "$JREVERSION" || true
        ln -s "$(find "$LAUDIR" -name java)" "$BIN"
        echo "Warning: SheepIt Link recreated, will redownload JRE on next launch"
    else
        echo "SheepIt Link found"
    fi
else
    BIN="./$APP"
fi
JAR="$APP.jar"
if [  ! -e ./$JAR  ]; then # If Jar exists in the dmg, don't download it
    URL=https://www.sheepit-renderfarm.com/media/applet/client-latest.php
    if [ -e "$LAUDIR/JOINBETA" ]; then
        URL=https://www.sheepit-renderfarm.com/media/applet/client-beta.php
    fi
    echo "Checking for $APP Jar updates"
    JAR_LATEST="$(curl --connect-timeout 20 --retry 4 --silent --show-error --head $URL | sed -nE 's|.*(sheepit.*\.jar).*|\1|p')"
    JAR="$LAUDIR/$JAR_LATEST"
        if [ -z "$JAR_LATEST" ]; then
            # Empty JAR_LATEST is a good indicator of a critical failure
            echo "!!! Failed parsing $APP Jar information! Aborting !!!"
            echo "Possible causes and troubleshooting steps:"
            echo "1. Check for internet connectivity \(Routes, DNS, Proxy\)"
            echo "2. Check the status of SheepIt via the SheepIt website: https://www.sheepit-renderfarm.com/"
            echo "3. Open an issue on GitLab/Ask on the Discord if problems persists after 1. and 2."
            exit 2
        elif [ ! -e "$JAR" ]; then
            echo "Updates detected, updating $APP Jar to $JAR_LATEST"
            rm -f "$LAUDIR/sheepit-client"* || true # The "|| true" is that we expect the command to fail (If there is nothing to remove), needed since we are running in strict mode
            curl --connect-timeout 20 --retry 4 -# -L -o "$JAR" "$URL"
        else
            echo "No $APP Jar updates found"
        fi
else
    JAR="./$JAR"
fi

echo "============================="
echo "SheepIt running on:"
sw_vers
echo "============================="
echo "Loading and launching SheepIt"
nohup "$BIN" -Xdock:icon=./$APP.png -Djna.nosys=true -jar "$JAR" &>/dev/null & disown
if [ "$DEBUG" = "0" ]; then
    osascript -e 'tell application "Terminal" to close (every window whose name contains "SheepIt-Run.sh")' &
fi
exit 0
