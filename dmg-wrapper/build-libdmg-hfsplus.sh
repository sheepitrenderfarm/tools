#!/bin/bash
set -euo pipefail # Unofficial strict mode, see http://redsymbol.net/articles/unofficial-bash-strict-mode/

cd "$(dirname "$(readlink -f "$0")")"
# see https://stackoverflow.com/questions/3349105/how-can-i-set-the-current-working-directory-to-the-directory-of-the-script-in-ba

sudo apt-get install git cmake build-essential zlib1g-dev

git clone https://github.com/fanquake/libdmg-hfsplus.git
(
    cd libdmg-hfsplus
    cmake . -B build
    make -C build/dmg -j8
    mv build/dmg/dmg ..
)
rm -rf libdmg-hfsplus/
